package com.masimpresiones.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.masimpresiones.app.network.HttpResponse;
import com.masimpresiones.app.network.OrdenesRequestAdapter;
import com.masimpresiones.app.util.MasPreferences;
import com.masimpresiones.app.util.MasStatus;

import org.json.JSONException;
import org.json.JSONObject;

public class RechazoActivity extends AppCompatActivity implements HttpResponse{

    private static final String TAG = "RECHAZOACTIVITY";

    // properties
    OrdenesRequestAdapter request;
    String area;
    String noorden;

    //data.
    Bundle extras;

    // ui.
    EditText etObs;
    Button btnRechazarConfirmacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rechazo);

        etObs = (EditText) findViewById(R.id.etRechazarObs);
        btnRechazarConfirmacion = (Button) findViewById(R.id.btnRechazarConfirmacion);
        MasPreferences.getInstance().removeCookie();
        //implementar httpresponse
        extras = getIntent().getExtras();
        if( extras != null && extras.get("noorden") != null && extras.get("area") != null ) {
            area = extras.getString("area");
            noorden = extras.getString("noorden");
        }
    }

    @Override
    public void onResponse(JSONObject json) {
        Log.i(TAG,"onResponse");
        //logged == true
        //go to list...

        try {
            boolean edited = json.getBoolean("edited");
            String message = json.getString("msg");
            if(edited) {
                Toast.makeText(this, "Orden rechazada.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }catch (JSONException e){
            Log.e(TAG,"error en respuesta " + e.toString());
        }

        Intent intent = new Intent(RechazoActivity.this, ListOrdenesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onError(String error) {
        Log.e(TAG,"error en peticion " + error.toString());
    }
}
