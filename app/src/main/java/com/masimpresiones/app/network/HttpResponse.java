package com.masimpresiones.app.network;

import org.json.JSONObject;

/**
 * Created by Programacion2 on 27/11/2015.
 */
public interface HttpResponse
{
    void onResponse(JSONObject json);
    void onError(String error);
}
