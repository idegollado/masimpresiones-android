package com.masimpresiones.app.network;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.android.volley.Response;
import com.masimpresiones.app.network.adapter.MasStringRequest;
import com.masimpresiones.app.util.MasPreferences;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Programacion2 on 27/11/2015.
 */
public class HttpRequestAdapter implements Response.Listener<String>,Response.ErrorListener
{

    private HttpResponse httpResponse;
    private RequestQueue volleyRequest;
    protected MasStringRequest stringRequest;



    /**
 * Este constructor hace la preparacion de la peticion al webservice
 * @param c1 Context
 * @param pResponse HttpResponse interface
 */
    public HttpRequestAdapter(Context c1, HttpResponse pResponse)
    {
        this.httpResponse = pResponse;
        volleyRequest = Volley.newRequestQueue(c1);

        String urlDefault = MasPreferences.getInstance().getHost() + "/masimpresiones/ordenes/index.json";
        stringRequest = new MasStringRequest(Request.Method.GET,urlDefault,this,this);
    }
    /**
     * Ejecuta la peticion al webservice y maneja la peticion.
     */
    public void enter()
    {
        RetryPolicy policy =
                new DefaultRetryPolicy(20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        this.stringRequest.setRetryPolicy(policy);
        this.volleyRequest.add(this.stringRequest);

    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        Log.e("HttpRequestAdapter", error.toString());
        httpResponse.onError(error.toString());
    }


    @Override
    public void onResponse(String response) {

        try {
            JSONObject jsonResponse = new JSONObject(response);
            httpResponse.onResponse(jsonResponse);
        }catch (JSONException e) {
            Log.e("HttpRequestAdapter", e.toString());
        }
    }
}
