package com.masimpresiones.app.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.masimpresiones.app.network.adapter.MasStringRequest;
import com.masimpresiones.app.util.MasPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Programacion2 on 27/11/2015.
 */
public class LoginRequestAdapter extends HttpRequestAdapter
{

    String user;
    String pass;


    public LoginRequestAdapter(Context c1, String usuario, String password, HttpResponse pResponse)
    {
        super(c1,pResponse);
        user = usuario;
        pass = password;
        stringRequest = createStringRequest();
    }


    public MasStringRequest getRequest()
    {
        return stringRequest;
    }

    private MasStringRequest createStringRequest() {
        //sobreescribiedo inline las interfaces...
        //int method, String url, JSONObject jsonRequest,
        // Response.Listener<JSONObject> listener, Response.ErrorListener errorListener)
        String loginUrl  = MasPreferences.getInstance().getHost() + "/masimpresiones/usuarios/andyLogin.json";
       return new MasStringRequest(Request.Method.POST, loginUrl, this, this)
       {
            @Override
             protected Map<String, String> getParams() throws AuthFailureError {
            /*Parametros que necesita la direccion url para poder hacer
            * la validacion*/
               Map<String, String> params = new HashMap<String, String>();
               params.put("usuario",user );
               params.put("contrasena",pass );

               return params;
           }
       };
    }


}
