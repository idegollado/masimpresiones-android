package com.masimpresiones.app.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.masimpresiones.app.network.adapter.MasStringRequest;
import com.masimpresiones.app.util.MasPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Programacion2 on 30/11/2015.
 */
public class OrdenesRequestAdapter extends HttpRequestAdapter {


    public OrdenesRequestAdapter(Context c1, HttpResponse pResponse) {
        super(c1, pResponse);

    }

    public void index() {
        // URLs
        String ordenesUrl   =
                MasPreferences.getInstance().getHost() + "/masimpresiones/usuarios/index.json";
        stringRequest = new MasStringRequest(Request.Method.POST, ordenesUrl,this,this);
        enter();
    }

    public void view(String area, String id) {

        String ordenViewUrl =
                MasPreferences.getInstance().getHost() + "/masimpresiones/" + area+ "/view/" + id + ".json";
        stringRequest = new MasStringRequest(Request.Method.POST,ordenViewUrl,this,this);

    }

    public void status(String area, String id){
        String ordenViewUrl =
                MasPreferences.getInstance().getHost() + "/masimpresiones/" + area+ "/status/" + area +"/"+ id;
        stringRequest = new MasStringRequest(Request.Method.POST,ordenViewUrl,this,this);

    }

}
