package com.masimpresiones.app.network.adapter;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.masimpresiones.app.util.MasPreferences;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

/**
 * Created by Programacion2 on 30/11/2015.
 */
public class MasStringRequest extends StringRequest
{
    public static final String TAG = "MASSTRINGREQUEST";

    public MasStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response)
    {
        //almacena cookies en las preferences..
        Map headers = response.headers;
        Log.i("MasStringRequest","parseNetworkResponse() " + headers.toString());
        String cookie = (String)headers.get("Set-Cookie");
        if(cookie != null && cookie != "")
            MasPreferences.getInstance().saveCookie(cookie);
        else
            Log.e("MasStringRequest", "No hay cookies");

        return super.parseNetworkResponse(response);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map headers = new HashMap();
        Log.i("MasStringRequest","getHeaders() " + headers.toString());
        if(!MasPreferences.getInstance().getCookie().equals(""))
            headers.put("Cookie", MasPreferences.getInstance().getCookie());
        else
            Log.w(TAG, "No hay cookies gurdadas");
        return headers != null ? headers : super.getHeaders();
    }

}
