package com.masimpresiones.app.ui.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.masimpresiones.app.ListOrdenesActivity;
import com.masimpresiones.app.LoginActivity;
import com.masimpresiones.app.MasImpresionesApp;
import com.masimpresiones.app.network.HttpResponse;
import com.masimpresiones.app.network.OrdenesRequestAdapter;

import org.json.JSONObject;

/**
 * Created by Programacion2 on 02/12/2015.
 */
public class MasAlertTerminarDialog extends DialogFragment{

    private static final String TAG = "MASALERTTERMINARDIALOG";

    private String message = "Una tarea terminada indica que has terminado la actividad con la orden ascociada a esta orden," +
            "Estas seguro que la tarea esta termianda?";

    OrdenesRequestAdapter request;
    String area, noorden;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        return builder.create();
    }

}
