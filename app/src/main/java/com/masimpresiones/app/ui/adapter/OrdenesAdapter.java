package com.masimpresiones.app.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.masimpresiones.app.R;
import com.masimpresiones.app.model.OrdenArea;
import com.masimpresiones.app.model.Ordenes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Programacion2 on 30/11/2015.
 */
public class OrdenesAdapter extends BaseAdapter implements ListAdapter {

    private static final String TAG = "ORDENESADAPTER";
    private final Activity activity;
    private final Ordenes ordenes;

    /*GUI*/
    LayoutInflater inflater;

    public OrdenesAdapter(Activity activity, Ordenes list) {
        this.activity = activity;
        this.ordenes = list;
    }

    @Override
    public int getCount() {
        return ordenes.getOrdenes().size();
    }

    @Override
    public OrdenArea getItem(int position) {
        return (OrdenArea)ordenes.getOrdenes().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(inflater == null )
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.layout_item, null);

            //relacionamos una variable java con los elementos descritos en xml.
            CardView cardViewItem = (CardView) convertView.findViewById(R.id.card_view);
            TextView cliente = (TextView) convertView.findViewById(R.id.tvCliente);
            TextView desc = (TextView) convertView.findViewById(R.id.tvDesc);
            TextView fecha = (TextView) convertView.findViewById(R.id.tvFecha);
            TextView noorden = (TextView) convertView.findViewById(R.id.tvNoorden);

            //Obtener la posicion un elemento de la lista para poder relacionar los datos
            OrdenArea orden = (OrdenArea) ordenes.getOrdenes().get(position);
            cliente.setText(orden.getCliente());
            desc.setText(orden.getDescripcion());
            fecha.setText(orden.getFecha());
            noorden.setText(String.valueOf(orden.getNoorden()));

            //cambiar color
            Calendar today = Calendar.getInstance();//fecha de hoy
            Calendar tomorrow = Calendar.getInstance();//fecha de manana
            tomorrow.add(Calendar.DATE,1);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


            //si las ordenes tienen la fecha de hoy o de ayer poner color rojo.
            //si son para manana poner en amarillo..
            long todayDays = TimeUnit.MILLISECONDS.toDays(today.getTimeInMillis());//obtener los dias completos hasta hoy
            long tomorrowDays = TimeUnit.MILLISECONDS.toDays(tomorrow.getTimeInMillis());//obtener los dias completos hasta manana
            try{
                if(TimeUnit.MILLISECONDS.toDays(dateFormat.parse(orden.getFecha()).getTime()) <=  todayDays)
                    cardViewItem.setCardBackgroundColor(Color.parseColor("#A80000"));
                else if(TimeUnit.MILLISECONDS.toDays(dateFormat.parse(orden.getFecha()).getTime()) ==  tomorrowDays)
                    cardViewItem.setCardBackgroundColor(Color.parseColor("#FFFF33"));
            }catch(ParseException e) {
                //System.out.println("Al parecer no se puede convertir el elemento " + (i + 1) + "en la posicion 0" );
            }



        }
        return convertView;
    }

    public String getArea(){
        return ordenes.getArea();
    }
}
