package com.masimpresiones.app;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.masimpresiones.app.model.help.OrdenesAreaHelper;
import com.masimpresiones.app.network.HttpResponse;
import com.masimpresiones.app.network.OrdenesRequestAdapter;
import com.masimpresiones.app.ui.dialog.MasAlertDialog;
import com.masimpresiones.app.ui.dialog.MasAlertTerminarDialog;

import org.json.JSONObject;
import java.util.Map;
import java.util.Set;

public class DetalleOrdenActivity extends AppCompatActivity implements HttpResponse{

    public static final String TAG = "DetalleOrdenActivity";
    //data
    String area, noorden;

    OrdenesRequestAdapter ordenesRequest;
    LinearLayout innersvlayout;
    Button btnTerminar;
    Button btnCancelar;
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_orden);
        innersvlayout = (LinearLayout) findViewById(R.id.mainLayout);
        //ESTA ACTIVITY VA A SER DINAMICA..
        btnTerminar = (Button) findViewById(R.id.btnTerminarOrden);

        //implementar httpresponse
        extras = getIntent().getExtras();
        if( extras != null && extras.get("noorden") != null && extras.get("area") != null ) {
            ordenesRequest = new OrdenesRequestAdapter(this, this);
            area = extras.getString("area");
            noorden = extras.getString("noorden");
            ordenesRequest.view(area, noorden);
            ordenesRequest.enter();
            Log.i(TAG,"View");
        }

    }

    @Override
    public void onResponse(JSONObject json) {

        OrdenesAreaHelper helper = new OrdenesAreaHelper();
        Map<String,String> mapDetalle = helper.parseJSONFilterExclude(json);
        Log.i(TAG, mapDetalle.toString());
        LayoutParams params =
                new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.setMargins(2, 2, 2, 2);

        LayoutParams textParams =
                new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
        textParams.weight = 1;

        GradientDrawable border = new GradientDrawable();
        border.setColor(0xFFCCCCCC);
        border.setStroke(1, 0xFF777777);


        //adapter
        for( Map.Entry<String, String> entry: mapDetalle.entrySet() )
        {
            LinearLayout subLayout  = new LinearLayout(this);
            subLayout.setOrientation(LinearLayout.HORIZONTAL);
            subLayout.setLayoutParams(textParams);
            subLayout.setBackground(border);
            TextView textViewKey = new TextView(this);
            textViewKey.setTextColor(Color.parseColor("#000000"));
            textViewKey.setTextSize(40);
           // textViewKey.setLayoutParams(params);
            textViewKey.setText(entry.getKey() + ":");

            TextView textViewValue = new TextView(this);
            textViewValue.setTextColor(Color.parseColor("#778899"));
            textViewValue.setTextSize(40);
            textViewValue.setText(entry.getValue());
            textViewValue.setLayoutParams(textParams);
            textViewValue.setGravity(Gravity.RIGHT);

            subLayout.addView(textViewKey);
            subLayout.addView(textViewValue);
            innersvlayout.addView(subLayout);
        }
    }

    @Override
    public void onError(String error) {

    }

    public void onClickStatus(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
       // Add the buttons
        builder.setMessage("Esta indica que ya ha terminado su tarea, " +
                "y desaparecera de la lista de ordenes pendientes. Desea marcar como terminada la orden?");
        builder.setPositiveButton("Si, estoy de acuerdo", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                ordenesRequest = new OrdenesRequestAdapter(DetalleOrdenActivity.this, getHttpResponseStatus());
                ordenesRequest.status(area, noorden);
                ordenesRequest.enter();
            }
        });
        builder.setNegativeButton("No estoy de acuerdo", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }


    public HttpResponse getHttpResponseStatus(){
        return new HttpResponse() {
            @Override
            public void onResponse(JSONObject json) {
                Log.i(TAG, "numero de area cambiado exitosamente");
                //enviar al menu de las listas--
                Intent intent = new Intent(DetalleOrdenActivity.this,ListOrdenesActivity.class);
                startActivity(intent);
            }

            @Override
            public void onError(String error) {

            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this,ListOrdenesActivity.class);
        startActivity(intent);
    }
}
