package com.masimpresiones.app.model;

import java.util.List;

/**
 * Created by Programacion2 on 01/12/2015.
 */
public class Ordenes {

    String area;
    List<OrdenArea> ordenes;

    public Ordenes(String area, List<OrdenArea> ordenes) {
        this.area = area;
        this.ordenes = ordenes;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public List<OrdenArea> getOrdenes() {
        return ordenes;
    }

    public void setOrdenes(List<OrdenArea> ordenes) {
        this.ordenes = ordenes;
    }
}
