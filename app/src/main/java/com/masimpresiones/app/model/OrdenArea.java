package com.masimpresiones.app.model;

/**
 * Created by Programacion2 on 30/11/2015.
 */
public class OrdenArea {

    private int noorden;
    private String fecha;
    private String descripcion;
    private String cliente;


    public OrdenArea(){}

    public OrdenArea(int noorden, String fecha, String descripcion, String cliente) {
        this.noorden = noorden;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.cliente = cliente;
    }

    public int getNoorden() {
        return noorden;
    }

    public void setNoorde(int noorde) {
        this.noorden = noorde;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }


}
