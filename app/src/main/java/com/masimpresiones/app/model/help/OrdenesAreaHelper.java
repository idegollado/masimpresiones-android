package com.masimpresiones.app.model.help;

import android.util.Log;

import com.masimpresiones.app.model.OrdenArea;
import com.masimpresiones.app.model.Ordenes;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by Programacion2 on 30/11/2015.
 */
public class OrdenesAreaHelper {

        public static final String TAG = "OrdenesAreaHelper";

        public String name;

        //DEVUELE LAS ORDENES  DE UN ALOJADAS EN UN JSON.
        public List parseJSON(JSONObject list) {
            //algoritmo para convertir json a ordenesarea..
            //almacenar el la ruta o algo
            //Log.i(TAG, list.names().toString());
            List<OrdenArea> ordenesAreaList = new ArrayList<>();

            try {
                JSONArray array = list.getJSONArray(list.names().getString(0));

                for (int i = 0; i < array.length(); i++) {

                    JSONObject tempObj = array.getJSONObject(i);
                    JSONObject detalleObj = tempObj.getJSONObject("ordenes_detalle");
                    JSONObject ordeneObj = tempObj.getJSONObject("ordene");

                    OrdenArea orden = new OrdenArea();
                    orden.setNoorde(tempObj.getInt("noorden"));
                    //Date Format.
                    Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(ordeneObj.getString("fechaEntrega"));
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String formatedDateString = sdf.format(date);

                    orden.setFecha(formatedDateString);
                    orden.setCliente(ordeneObj.getString("nombreCliente"));//error
                    orden.setDescripcion(detalleObj.getString("descripcion"));

                    ordenesAreaList.add(orden);
                }
            } catch (Exception e){
                Log.e(TAG, e.toString());
            }
            Log.i(TAG, "Parse Ok!");
            return ordenesAreaList;
        }

        public Ordenes parseJSONOrdenes(JSONObject list) {
        //algoritmo para convertir json a ordenesarea..
        //almacenar el la ruta o algo
        //Log.i(TAG, list.names().toString());

        List<OrdenArea> ordenesAreaList = new ArrayList<>();
        String areaName = "";
        try {
            areaName = list.names().getString(0);
            JSONArray array = list.getJSONArray(areaName);
            Log.i(TAG, list.toString());
            for (int i = 0; i < array.length(); i++) {

                JSONObject tempObj = array.getJSONObject(i);
                JSONObject detalleObj = tempObj.getJSONObject("ordenes_detalle");
                JSONObject ordeneObj = tempObj.getJSONObject("ordene");

                OrdenArea orden = new OrdenArea();
                orden.setNoorde(tempObj.getInt("noorden"));
                //Date Format.
                Log.i(TAG, ordeneObj.getString("fechaEntrega"));
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");//el formato con la Z al ultimo se desfasa un dia..
                Date date = dateFormat.parse(ordeneObj.getString("fechaEntrega"));
                Log.i(TAG, date.toString());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String formatedDateString = sdf.format(date);


                orden.setFecha(formatedDateString);
                orden.setCliente(ordeneObj.getString("nombreCliente"));//error
                orden.setDescripcion(detalleObj.getString("descripcion"));
                ordenesAreaList.add(orden);
            }
        } catch (Exception e){
            Log.e(TAG, e.toString());
        }
        Log.i(TAG, "Parse, Ok!");
            Ordenes ordenes = new Ordenes(areaName, ordenesAreaList);
        return ordenes;
    }
        /*
         * return a list of key,value of diferents tables of orders.
         *
         */
        public Map<String,String> parseJSONNames(JSONObject json) {
            Map<String,String> detalles = new HashMap<String,String>();
            try {
                String rootName =  json.names().getString(0);
                JSONObject rootJson = json.getJSONObject(rootName);
                JSONObject ordenJson = rootJson.getJSONObject("ordene");
                detalles.put(ordenJson.names().getString(1),ordenJson.getString(ordenJson.names().getString(1)));

                JSONObject ordenDetalleJson = rootJson.getJSONObject("ordenes_detalle");
                detalles.put(ordenDetalleJson.names().getString(4),ordenDetalleJson.getString(ordenDetalleJson.names().getString(4)));
                //get detalles
                //get orden afuera del for..

                for(int i=0;i < rootJson.length() - 2;i++) {
                    //  return rootJson.names().toString();
                    detalles.put(rootJson.names().getString(i), rootJson.getString(rootJson.names().getString(i)));
                }

            }catch (Exception e){
                return null;
            }

            return detalles;
        }

        public Map<String,String> parseJSONFilterExclude(JSONObject json) {
            Map<String,String> detalles = new HashMap<String,String>();
            try{
                //root es el nombre las tablas que regularmente es dinamico (ordenes_impresiones, ordenes_cerigrafia)
                String rootName = json.names().getString(0);
                Log.i(TAG, rootName);
                //obtener el contenido de ...el elemento root.
                JSONObject rootJson = json.getJSONObject(rootName);
                Log.i(TAG, rootJson.toString());
                //filtrar el contenido
               for(int i=0;i < rootJson.length();i++) {
                    //algun valor tiene un false? no lo agregues

                    if (!(rootJson.getString(rootJson.names().getString(i)).equals("false"))) {
                        if(!rootJson.names().getString(i).equals("ordene") ) {
                            if (!rootJson.names().getString(i).equals("ordenes_detalle")) {
                                detalles.put(rootJson.names().getString(i), rootJson.getString(rootJson.names().getString(i)));
                            }
                        }
                    }
               }

                JSONObject ordenJson = rootJson.getJSONObject("ordene");
                //get fecha
               detalles.put("via",ordenJson.getString("via"));

                JSONObject ordenDetalleJson = rootJson.getJSONObject("ordenes_detalle");
                //get descripcion..
               detalles.put("cantidad", ordenDetalleJson.getString("cant"));

                return detalles;
            }catch (Exception e){

                return null;
            }
        }
}
