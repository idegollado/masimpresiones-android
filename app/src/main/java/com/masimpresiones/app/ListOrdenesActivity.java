package com.masimpresiones.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.masimpresiones.app.model.OrdenArea;
import com.masimpresiones.app.model.Ordenes;
import com.masimpresiones.app.model.help.OrdenesAreaHelper;
import com.masimpresiones.app.network.HttpResponse;
import com.masimpresiones.app.network.OrdenesRequestAdapter;
import com.masimpresiones.app.ui.adapter.OrdenesAdapter;
import com.masimpresiones.app.ui.dialog.MasAlertDialog;

import org.json.JSONObject;

import java.util.List;

public class ListOrdenesActivity extends AppCompatActivity
        implements HttpResponse,AdapterView.OnItemClickListener{

    public static final String TAG = "ListOrdenesActiviy";

    OrdenesRequestAdapter ordenesRequest;
    OrdenesAdapter dataAdapter;
    Ordenes ordenesbyArea;
    // UI References.
    ListView listView;
    LinearLayout activityListOrdenesLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_ordenes);
        activityListOrdenesLayout = (LinearLayout) findViewById(R.id.activity_list_ordenes);
        listView = (ListView) findViewById(R.id.listViewOrdenes);

        ordenesRequest =
                new OrdenesRequestAdapter(
                        ListOrdenesActivity.this,
                        ListOrdenesActivity.this);
        ordenesRequest.index();

        listView.setOnItemClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                // User chose the "Settings" item, show the app settings UI...
                MasAlertDialog alertDialog = new MasAlertDialog();
                alertDialog.show(getFragmentManager(), "MasAlertDialog");
                return true;
            case R.id.action_update:
                ordenesRequest = new OrdenesRequestAdapter(
                                ListOrdenesActivity.this,
                                ListOrdenesActivity.this);
                ordenesRequest.index();
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public void onResponse(JSONObject json)
    {
        //decodificar el adapter y mostrar la lista...
        Log.i("OrdenesActivity", json.toString());
        OrdenesAreaHelper helper = new OrdenesAreaHelper();
        ordenesbyArea = helper.parseJSONOrdenes(json);
        if( ordenesbyArea.getOrdenes().size() > 0) {
            //adapter
            dataAdapter = new OrdenesAdapter(this, ordenesbyArea);
            listView.setAdapter(dataAdapter);
        }else {
            ViewGroup.LayoutParams textParams =
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            TextView textView = new TextView(this);
            textView.setText("No hay ninguna tarea asignada por ahora.");
            textView.setGravity(Gravity.CENTER);
            textView.setLayoutParams(textParams);
            textView.setTextSize(30);
            activityListOrdenesLayout.addView(textView);
        }

    }

    @Override
    public void onError(String error)
    {
        Log.i(this.getLocalClassName(),error);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        OrdenArea orden = (OrdenArea)parent.getItemAtPosition(position);
        String area = ordenesbyArea.getArea();
        Log.i(TAG, String.valueOf(orden.getNoorden()));

        Intent intent = new Intent(this, DetalleOrdenActivity.class);
        intent.putExtra("noorden", String.valueOf(orden.getNoorden()));
        intent.putExtra("area",area);
        startActivity(intent);

    }
}
