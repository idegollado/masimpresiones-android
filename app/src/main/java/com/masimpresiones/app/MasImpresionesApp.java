package com.masimpresiones.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by Programacion2 on 27/11/2015.
 */
public class MasImpresionesApp extends Application
{

    private static Context context;

    @Override
    public void onCreate()
    {
        super.onCreate();
        context = this.getApplicationContext();

    }

    public static Context getContext()
    {
        return MasImpresionesApp.context;
    }
}
