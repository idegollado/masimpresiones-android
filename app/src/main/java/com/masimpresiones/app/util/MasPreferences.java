package com.masimpresiones.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.masimpresiones.app.MasImpresionesApp;

/**
 * Created by Programacion2 on 30/11/2015.
 */
public class MasPreferences  {
    private static MasPreferences ourInstance = new MasPreferences();

    public static MasPreferences getInstance() {
        return ourInstance;
    }

    private MasPreferences() {
    }

    public void saveCookie(String cookie) {
        if (cookie == null) {
            Log.e("Preferences", "cookie value doesn't exist");
            //the server did not return a cookie so we wont have anything to save
            return;
        }
        // Save in the preferences
        SharedPreferences prefs = getPreferences("Cookie");
        if (null == prefs) {
            Log.e("Preferences", "cookie file doesn't exist");
            return;
        }
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("cookie", cookie);
        editor.commit();
    }

    public String getCookie() {
        SharedPreferences prefs = getPreferences("Cookie");
        String cookie = prefs.getString("cookie", "");
        if (cookie.contains("expires")) {
        /** you might need to make sure that your cookie returns expires when its expired. I also noted that cokephp returns deleted */
            removeCookie();
            return "";
        }
        return cookie;
    }

    public void removeCookie() {

        SharedPreferences prefs = getPreferences("Cookie");
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("cookie");
        editor.commit();
        Log.e("Preferences", "cookie removed");
    }

    public SharedPreferences getPreferences(String key) {
        return MasImpresionesApp.getContext().getSharedPreferences(key, Context.MODE_PRIVATE);
    }


    public void saveHost(String pHost){
        if (pHost == null) {
            Log.e("Preferences", "HOST es null");
            //the server did not return a cookie so we wont have anything to save
            return;
        }
        // Save in the preferences
        SharedPreferences prefs = getPreferences("Host");
        if (null == prefs) {
            Log.e("Preferences", "Host file doesn't exist");
            return;
        }
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("host",pHost);
        editor.commit();
    }
    public String getHost(){
        SharedPreferences prefs = getPreferences("Host");
        String cookie = prefs.getString("host", "");

        return cookie;
    }

    public void saveUser(String pUser){
        if(pUser == null) {
            Log.e("Preferences", "No hay usuario que guardar");
            return;
        }

        SharedPreferences preferences = getPreferences("Auth");
        if (null == preferences){
            Log.e("Preferences", "Auth file doesn't exist");
            return;
        }

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("user",pUser);
        editor.commit();

    }
    public String getUser(){
        SharedPreferences prefs = getPreferences("Auth");
        String user = prefs.getString("user", "");
        return user;
    }
    public void savePassword(String pPass){
        if(pPass == null) {
            Log.e("Preferences", "No hay password que guardar");
            return;
        }

        SharedPreferences preferences = getPreferences("Auth");
        if (null == preferences){
            Log.e("Preferences", "Auth file doesn't exist");
            return;
        }

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("password",pPass);
        editor.commit();

    }
    public String getPassword(){
        SharedPreferences prefs = getPreferences("Auth");
        String password = prefs.getString("password","");
        return password;
    }
}
