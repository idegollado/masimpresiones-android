package com.masimpresiones.app.util;

/**
 * Created by Programacion2 on 03/12/2015.
 */
public class MasStatus {
    public static final int PENDIENTE = 0;
    public static final  int TERMINADA = 1;
    public static final int RECHAZADA = 2;
}
