package com.masimpresiones.app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.masimpresiones.app.network.HttpResponse;
import com.masimpresiones.app.network.LoginRequestAdapter;
import com.masimpresiones.app.network.OrdenesRequestAdapter;
import com.masimpresiones.app.ui.dialog.MasAlertDialog;
import com.masimpresiones.app.util.MasPreferences;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements HttpResponse {



    private LoginRequestAdapter loginRequest;

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mEmailView.setText(MasPreferences.getInstance().getUser());
        mPasswordView.setText(MasPreferences.getInstance().getPassword());
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
              //hacer peticion de login.
                Log.i("LoginActivity", "onClick LOGIN");
                //no debe haber sessiones multiples para un mismo dispositivo..
                MasPreferences.getInstance().removeCookie();
                MasPreferences.getInstance().saveUser(mEmailView.getText().toString());
                MasPreferences.getInstance().savePassword(mPasswordView.getText().toString());
                loginRequest =
                        new LoginRequestAdapter(
                                LoginActivity.this,
                                mEmailView.getText().toString(),
                                mPasswordView.getText().toString(),
                                LoginActivity.this);
                loginRequest.enter();

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_host, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_config:
                Intent intent = new Intent(this,ConfigActivity.class);
                startActivity(intent);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onResponse(JSONObject json)
    {
        Log.i("LoginActivity","onResponse");
        //logged == true
        //go to list...

        try {
            boolean logged = json.getBoolean("logged");
            if(logged) {
                Intent intent = new Intent(LoginActivity.this, ListOrdenesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                Toast.makeText(this,"Los datos son incorrectos", Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e){
            Log.e("LoginActiviy","error al codificar la peticion");
            Toast.makeText(this,"Verifique los datos en el menu CONFIG", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onError(String error)
    {
        Log.i(this.getLocalClassName(), error);
        Toast.makeText(this,"Verifique los datos en el menu CONFIG", Toast.LENGTH_LONG).show();
    }


}

