package com.masimpresiones.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.masimpresiones.app.util.MasPreferences;

public class ConfigActivity extends AppCompatActivity {

    public static  final String TAG = "CONFIGACTIVITY";

    // ui
    EditText etDomain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        etDomain = (EditText) findViewById(R.id.et_domain);
        etDomain.setText(MasPreferences.getInstance().getHost());

    }

    public void onClickGuardarConfig(View view) {
        MasPreferences.getInstance().saveHost(etDomain.getText().toString());

        if( MasPreferences.getInstance().getHost() != null) {
            Toast.makeText(this, "Datos guardados correctamente", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            Toast.makeText(this, "Datos no guardados", Toast.LENGTH_LONG).show();
        }
    }

}
